open Treeprint
open Treeprint.OCaml

let num z = string (string_of_int z)
let id x = string x
let int = id "int"
let alpha = id "'a"

let test t answer =
  let str = Token.show (t Noassoc 0.0) in
  Format.eprintf "%s =?= %s@." str answer;
  if str <> answer then failwith "FAILED";
  Format.eprintf "%a@.@." Token.format  (t Noassoc 0.0);
  Format.eprintf "%a@.@." Token.dump  (t Noassoc 0.0)

let test () =
  test (num 0) "0";
  test (num 0 + num 0) "0 + 0";
  test (num 0 + num 0 * num 0) "0 + 0 * 0";
  test (num 0 * (num 0 + num 0)) "0 * (0 + 0)";
  test ((num 1 + num 2) * num 3) "(1 + 2) * 3";
  test (num 1 + (num 2 + num 3)) "1 + (2 + 3)"; (* It is as same as 1 + 2 + 3 but just semantically *)
  test (num 1 - (num 2 - num 3)) "1 - (2 - 3)";
  test (num 1 - num 2 - num 3) "1 - 2 - 3";
  test (uminus (num 1)) "-1";
  test (uminus (num 1 + num 2 + num 3))   "-(1 + 2 + 3)";
  test (num 1 + uminus (num 1)) "1 + -1";
  test (uminus (uminus (num 1))) "- -1";
  test (int ^-> int ^-> int) "int -> int -> int";
  test ((int ^-> int) ^-> int) "(int -> int) -> int";
  test (ty_as (int ^-> int ^-> int) alpha) "int -> int -> int as 'a";
  test ((ty_as (int ^-> int) alpha) ^-> int) "(int -> int as 'a) -> int";
  test (tuple [num 1; num 2; num 3]) "1, 2, 3";
  test (tuple [num 1; tuple [num 2; num 3]; num 4]) "1, (2, 3), 4";
  test (app (app (id "x") (id "y")) (id "z")) "x y z";
  test (app (id "x") (app (id "y") (id "z"))) "x (y z)";
  test (app (id "x") (num 1 * num 2)) "x (1 * 2)";
  test (sequence [ num 1 + num 2; num 1 + num 2; num 1 + num 2 ]) "1 + 2; 1 + 2; 1 + 2";
  test (if_then_else (num 1 + num 2) (num 1 + num 2) (num 1 + num 2)) "if 1 + 2 then 1 + 2 else 1 + 2";
  test (if_then_else (num 1 + num 2) (num 1 + num 2) (num 1) + num 2) "(if 1 + 2 then 1 + 2 else 1) + 2";
  test (app (if_then_else (num 1 + num 2) (num 1 + num 2) (num 1)) (num 2)) "(if 1 + 2 then 1 + 2 else 1) 2";
  test (app (id "f") (if_then_else (num 1 + num 2) (num 1 + num 2) (num 1 + num 2))) "f (if 1 + 2 then 1 + 2 else 1 + 2)";
  test (if_then_else
          (sequence [ num 1 + num 2; num 1 + num 2 ])
          (sequence [ num 1 + num 2; num 1 + num 2 ])
          (sequence [ num 1 + num 2; num 1 + num 2 ])) "if 1 + 2; 1 + 2 then (1 + 2; 1 + 2) else (1 + 2; 1 + 2)";
  test (sequence [if_then_else
               (sequence [ num 1 + num 2; num 1 + num 2 ])
               (sequence [ num 1 + num 2; num 1 + num 2 ])
               (sequence [ num 1 + num 2; num 1 + num 2 ]);
             num 1 + num 2 ]) "if 1 + 2; 1 + 2 then (1 + 2; 1 + 2) else (1 + 2; 1 + 2); 1 + 2";
  test (if_then
          (sequence [ num 1 + num 2; num 1 + num 2 ])
          (sequence [ num 1 + num 2; num 1 + num 2 ])) "if 1 + 2; 1 + 2 then (1 + 2; 1 + 2)";
  test (sequence [if_then
               (sequence [ num 1 + num 2; num 1 + num 2 ])
               (sequence [ num 1 + num 2; num 1 + num 2 ]);
             num 1 + num 2 ]) "if 1 + 2; 1 + 2 then (1 + 2; 1 + 2); 1 + 2";
  prerr_endline "done"

let () = test ()
